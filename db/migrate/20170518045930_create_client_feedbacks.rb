class CreateClientFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :client_feedbacks do |t|
    	t.integer :client_id 
    	t.string :feedback 

        t.timestamps

    end
  end
end

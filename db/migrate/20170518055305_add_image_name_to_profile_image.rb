class AddImageNameToProfileImage < ActiveRecord::Migration[5.0]


  	def self.up
    	add_column :profile_images, :image_name,    :string
     	
    end
  
    def self.down
    	remove_column :profile_images, :image_name
 
    end
end

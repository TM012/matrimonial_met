class CreateProfileTrustBadges < ActiveRecord::Migration[5.0]
  def change
    create_table :profile_trust_badges do |t|
    	t.integer :profile_id
    	t.integer  :trust_value
      	t.timestamps
    end
  end
end

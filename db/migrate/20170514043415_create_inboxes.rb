class CreateInboxes < ActiveRecord::Migration[5.0]
  def change
    create_table :inboxes do |t|
        t.integer :sent_profile_id
        t.integer :receive_profile_id
    	  t.string  :message
    	  t.integer :sent_message_seen
    	  t.integer :receive_message_seen


      	t.timestamps
    end
  end
end

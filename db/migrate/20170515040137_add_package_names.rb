class AddPackageNames < ActiveRecord::Migration[5.0]
  def change

  	PackageList.create(package_name: "Silver",duration_unit: "month",price: 25 ,duration: 1)
  	PackageList.create(package_name: "Silver Plus",duration_unit: "month",price: 35 ,duration: 1)

  	PackageList.create(package_name: "Gold",duration_unit: "month",price: 50 ,duration: 3)
  	PackageList.create(package_name: "Gold Plus",duration_unit: "month",price: 60 ,duration: 3)

  	PackageList.create(package_name: "Diamond",duration_unit: "month",price: 75 ,duration: 6)
  	PackageList.create(package_name: "Diamond Plus",duration_unit: "month",price: 100 ,duration: 6)
  
  	PackageList.create(package_name: "Platinum",duration_unit: "month",price: 150 ,duration: 12)
  	PackageList.create(package_name: "Platinum Plus",duration_unit: "month",price: 175 ,duration: 12)

  	
  end
end




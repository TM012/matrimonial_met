class CreateProfileAcceptedLists < ActiveRecord::Migration[5.0]
  def change
    create_table :profile_accepted_lists do |t|
    	t.integer :profile_id
    	t.string  :id_list

      	t.timestamps
    end
  end
end

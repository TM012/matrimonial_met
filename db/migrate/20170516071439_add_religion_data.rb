class AddReligionData < ActiveRecord::Migration[5.0]
  def change

  		Religion.create(religion_name: "Muslim")
  		Religion.create(religion_name: "Hindu")
  		Religion.create(religion_name: "Chirstian")
  		Religion.create(religion_name: "Sikh")
  		Religion.create(religion_name: "Parsi")
  		Religion.create(religion_name: "Jain")
  		Religion.create(religion_name: "Buddhist")
  		Religion.create(religion_name: "Jewish")
  		Religion.create(religion_name: "Spiritual")
  		Religion.create(religion_name: "Atheism")
  		Religion.create(religion_name: "Other")
  end
end

class AddFreeAccessToClient < ActiveRecord::Migration[5.0]
  def change
  	add_column :clients, :free_access, :integer , :default => 0
  end
end

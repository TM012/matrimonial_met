class CreatePackageLists < ActiveRecord::Migration[5.0]
  def change
    create_table :package_lists , id: false do |t|

    	t.primary_key :package_id
    	t.float   :duration
    	t.string  :package_name
    	t.string  :package_details
    	t.string  :price
    	t.string :duration_unit
      	t.timestamps
    end
  end
end

class CreateProfileMusicTypeLists < ActiveRecord::Migration[5.0]
  def change
    create_table :profile_music_type_lists do |t|
    	t.integer :profile_id
    	t.string  :music_type
      	t.timestamps
    end
  end
end

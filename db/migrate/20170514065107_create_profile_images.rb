class CreateProfileImages < ActiveRecord::Migration[5.0]
  def change
    create_table :profile_images do |t|
    	t.integer :profile_id

        t.timestamps
    end
  end
end

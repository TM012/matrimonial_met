class CreateProfileHobbyLists < ActiveRecord::Migration[5.0]
  def change
    create_table :profile_hobby_lists do |t|
    	t.integer :profile_id
    	t.string  :hobby_name

      	t.timestamps
    end
  end
end

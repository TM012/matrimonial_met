class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|

    	t.integer :country_id
        t.string :country_name
        t.string :shortname
        t.string :phonecode
        
    end
  end
end

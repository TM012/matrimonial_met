class AddPhotoToClientFeedack < ActiveRecord::Migration[5.0]
	def self.up
    	add_column :client_feedbacks, :profile_photo_file_name,    :string
     	add_column :client_feedbacks, :profile_photo_content_type, :string
      	add_column :client_feedbacks, :profile_photo_file_size,    :integer
      	add_column :client_feedbacks, :profile_photo_updated_at,   :datetime
    end
  
    def self.down
    	remove_column :client_feedbacks, :profile_photo_file_name
     	remove_column :client_feedbacks, :profile_photo_content_type
     	remove_column :client_feedbacks, :profile_photo_file_size
     	remove_column :client_feedbacks, :profile_photo_updated_at
    end
end

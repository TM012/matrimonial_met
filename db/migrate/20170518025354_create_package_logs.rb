class CreatePackageLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :package_logs do |t|
    	t.string :package_name
    	t.integer :client_id
    	t.date :start_date
    	t.date :end_date



      	t.timestamps
    end
  end
end

class AddStatusToProfileTrustBadgesDocList < ActiveRecord::Migration[5.0]
  def change
    add_column :profile_trust_badges_doc_lists, :status, :string , :default => "Not verified"
    
  end
end

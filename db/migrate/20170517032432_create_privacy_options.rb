class CreatePrivacyOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :privacy_options do |t|
      t.integer :profile_id 
      t.integer :display_name_status , :default => 1
      t.integer :profile_visible_registered_status , :default => 0
      t.integer :profile_visible_all_status , :default => 1
      t.integer :profile_hide_status , :default => 0

      t.timestamps
    end
  end
end

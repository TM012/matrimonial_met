class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients , id: false  do |t|
    	t.primary_key :client_id
    	t.string  :email
    	t.string  :password
    	t.string  :relation_to_profile
    	t.string  :client_status
    	t.string  :package_name
    	



      t.timestamps
    end
  end
end

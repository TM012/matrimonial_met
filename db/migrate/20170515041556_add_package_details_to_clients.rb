class AddPackageDetailsToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :package_start_date, :date
    add_column :clients, :package_end_date, :date
  end
end

class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles , id: false do |t|

    	t.primary_key :profile_id
        t.integer :client_id
    	t.string :official_id
    	t.string :first_name
    	t.string :last_name
    	t.integer :age
    	t.string :gender
    	t.date 	 :dob
    	t.string :religion
    	t.string :mother_tongue
    	t.string :other_languages
    	t.string :living_in
    	t.string :diet
    	t.integer :smoking_status
    	t.integer :drinking_status
    	t.float :height_meter
    	t.string :body_type
    	t.string :father_name
    	t.string :mother_name
    	t.integer :number_of_brothers
    	t.integer :number_of_sisters
    	t.string :mobile_no
    	t.integer :facebook_sync_status
    	t.string :education_level
    	t.string :education_field
    	t.string :work_with
    	t.string :work_as
    	t.float :annual_income
    	t.string :about_myself
    	t.string :disability
    	t.string :live_in
    	t.string :marital_status
        t.string :facebook_image_link
    	t.integer :number_of_children



    	t.timestamps
    end
  end
end

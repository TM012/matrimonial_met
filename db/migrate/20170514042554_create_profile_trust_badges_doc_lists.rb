class CreateProfileTrustBadgesDocLists < ActiveRecord::Migration[5.0]
  def change
    create_table :profile_trust_badges_doc_lists do |t|
        t.integer :profile_id
    	t.string  :doc_name

      	t.timestamps
    end
  end
end

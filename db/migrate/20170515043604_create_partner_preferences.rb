class CreatePartnerPreferences < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_preferences do |t|

    	t.integer :profile_id

    	
    	t.integer :age_start
    	t.integer :age_end

    	t.string :diet
    	t.integer :smoking_status
    	t.integer :drinking_status
    	t.float :height_meter_start
    	t.float :height_meter_end
    	t.string :body_type
    	
    	t.string :education_level
    	t.string :education_field

    	t.string :live_in
    	
    	t.string :marital_status
    	t.string :work_with
    	t.float :annual_income


      	t.timestamps
    end
  end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170518055305) do

  create_table "client_feedbacks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.string   "feedback"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "profile_photo_file_name"
    t.string   "profile_photo_content_type"
    t.integer  "profile_photo_file_size"
    t.datetime "profile_photo_updated_at"
  end

  create_table "clients", primary_key: "client_id", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email"
    t.string   "password"
    t.string   "relation_to_profile"
    t.string   "client_status"
    t.string   "package_name"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.date     "package_start_date"
    t.date     "package_end_date"
    t.integer  "free_access",         default: 0
  end

  create_table "countries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "country_id"
    t.string  "country_name"
    t.string  "shortname"
    t.string  "phonecode"
  end

  create_table "country_areas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "country_id"
    t.string  "area_name"
  end

  create_table "inboxes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "sent_profile_id"
    t.integer  "receive_profile_id"
    t.string   "message"
    t.integer  "sent_message_seen"
    t.integer  "receive_message_seen"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "languages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "language_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "package_lists", primary_key: "package_id", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float    "duration",        limit: 24
    t.string   "package_name"
    t.string   "package_details"
    t.string   "price"
    t.string   "duration_unit"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "package_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "package_name"
    t.integer  "client_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "partner_preferences", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.integer  "age_start"
    t.integer  "age_end"
    t.string   "diet"
    t.integer  "smoking_status"
    t.integer  "drinking_status"
    t.float    "height_meter_start", limit: 24
    t.float    "height_meter_end",   limit: 24
    t.string   "body_type"
    t.string   "education_level"
    t.string   "education_field"
    t.string   "live_in"
    t.string   "marital_status"
    t.string   "work_with"
    t.float    "annual_income",      limit: 24
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "privacy_options", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.integer  "display_name_status",               default: 1
    t.integer  "profile_visible_registered_status", default: 0
    t.integer  "profile_visible_all_status",        default: 1
    t.integer  "profile_hide_status",               default: 0
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "profile_accepted_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.string   "id_list"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profile_hobby_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.string   "hobby_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profile_images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "additional_profile_photo_file_name"
    t.string   "additional_profile_photo_content_type"
    t.integer  "additional_profile_photo_file_size"
    t.datetime "additional_profile_photo_updated_at"
    t.string   "image_name"
  end

  create_table "profile_invitations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.string   "id_list"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profile_music_type_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.string   "music_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profile_rejected_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.string   "id_list"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profile_trust_badges", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.integer  "trust_value"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "profile_trust_badges_doc_lists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "profile_id"
    t.string   "doc_name"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "attach_trust_file_name"
    t.string   "attach_trust_content_type"
    t.integer  "attach_trust_file_size"
    t.datetime "attach_trust_updated_at"
    t.string   "status",                    default: "Not verified"
  end

  create_table "profiles", primary_key: "profile_id", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "client_id"
    t.string   "official_id"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "age"
    t.string   "gender"
    t.date     "dob"
    t.string   "religion"
    t.string   "mother_tongue"
    t.string   "other_languages"
    t.string   "living_in"
    t.string   "diet"
    t.integer  "smoking_status"
    t.integer  "drinking_status"
    t.float    "height_meter",               limit: 24
    t.string   "body_type"
    t.string   "father_name"
    t.string   "mother_name"
    t.integer  "number_of_brothers"
    t.integer  "number_of_sisters"
    t.string   "mobile_no"
    t.integer  "facebook_sync_status"
    t.string   "education_level"
    t.string   "education_field"
    t.string   "work_with"
    t.string   "work_as"
    t.float    "annual_income",              limit: 24
    t.string   "about_myself"
    t.string   "disability"
    t.string   "live_in"
    t.string   "marital_status"
    t.string   "facebook_image_link"
    t.integer  "number_of_children"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "profile_photo_file_name"
    t.string   "profile_photo_content_type"
    t.integer  "profile_photo_file_size"
    t.datetime "profile_photo_updated_at"
  end

  create_table "religions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "religion_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "web_profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "about_us"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end

// Unobtrusive RESTful dynamic/dependent select menus for Ruby on Rails 3 and jQuery
//
// USAGE (with Formtastic):
//
// match   = form.object
// seasons = Season.all
// rounds  = match.season.nil? ? Array.new : match.season.rounds
//
// form.input :season, :as => :select, :collection => seasons, :include_blank => false, :prompt => true, :input_html => { :id => :season_id }
// form.input :round,  :as => :select, :collection => rounds,  :include_blank => false, :prompt => true, :input_html => { :id => :round_id,
//   "data-option-dependent"    => true,
//   "data-option-observed"     => "season_id",
//   "data-option-url"          => "/seasons/:season_id:/rounds.json",
//   "data-option-key-method"   => :id,
//   "data-option-value-method" => :name
// }
//
// JSON response example:
// [{"end_date":"2011-10-29","name":"First","start_date":"2011-10-01","state":"opened"},
// {"end_date":"2011-09-30","name":"Second","start_date":"2011-09-03","state":"opened"}]

jQuery(document).ready(function() {
    $('select[data-option-dependent=true]').each(function (i) {
        console.log('yyyyyyyy');
       
        var observer_dom_id = $(this).attr('id');
        var observed_dom_id = $(this).data('option-observed');
        var url_mask        = $(this).data('option-url');
         
        //var key_method      = "lot_id"; //$(this).data('option-key-method');
        //var value_method    = "lot_number"; //$(this).data('option-value-method');
        var key_method      = $(this).data('option-key-method');
        var value_method    =  $(this).data('option-value-method');
//        var prompt          = $(this).has('option[value=]').size() ? $(this).find('option[value=]') : $('<option>').text('?');
        var regexp          = /:[0-9a-zA-Z_]+/g;

        var observer = $('select#'+ observer_dom_id);
        var observed = $('select#'+ observed_dom_id);

        if (!observer.val() && observed.size() > 1) {
            observer.attr('disabled', true);
        }
        observed.on('change', function() {
            url = url_mask.replace(regexp, function(submask) {
                dom_id = submask.substring(1, submask.length);
                return $("select#"+ dom_id).val();
            });

            observer.empty();//.append(prompt);

            $.getJSON(url, function(data) {
                $.each(data, function(i, object) {
                    observer.append($('<option>').attr('value', object[key_method]).text(object[value_method]));
                    observer.attr('disabled', false);
                });
               // $('select').material_select();
            });
        });
    });
});





$(function() {

var form = $("#example-advanced-form").show();
 
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    onStepChanging: function (event, currentIndex, newIndex)
    {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex)
        {
            return true;
        }
        // Forbid next action on "Warning" step if the user is to young
        if (newIndex === 3 && Number($("#age-2").val()) < 18)
        {
            return false;
        }
        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex)
        {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onStepChanged: function (event, currentIndex, priorIndex)
    {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
        {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3)
        {
            form.steps("previous");
        }
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
    }
}).validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        confirm: {
            equalTo: "#password-2"
        }
    }
});


})
;

class InboxesController < ApplicationController

  def sent_message
    cipher = Gibberish::AES.new('p4ssw0rd')
    client_id = id = cipher.decrypt(session[:client_id])
    profile_id = Profile.where(:client_id => id.to_i).limit(1).pluck(:profile_id).try(:first).to_i


    
    unless params[:profile_id].blank?
      @receive_profile_id = params[:profile_id]
      
    end
    #check if the sender is allowed to sent the message to the recipient
    list_string =""

    @get_rejected = ProfileRejectedList.where(:profile_id => params[:profile_id].to_i)
    res = @get_rejected[0].profile_id.nil? rescue true
    unless res
        i = 0
        loop do

          list_string = list_string.to_s + @get_rejected[i].id_list.to_s

            i += 1
            if i >= @get_rejected.length.to_i

              array_rejected_list = list_string.split(',').uniq
              break       # this will cause execution to exit the loop
            end
        end
    else

      array_rejected_list = []

    end


    unless array_rejected_list.include? profile_id
    
      Inbox.where(:sent_profile_id =>  params[:profile_id] ,:receive_profile_id =>profile_id ).update_all(receive_message_seen: 1)

      @message_list = []

      receive_profile_first_name = Profile.where(:profile_id => params[:profile_id].to_i).limit(1).pluck(:first_name).try(:first).to_s
      receive_profile_last_name = Profile.where(:profile_id => params[:profile_id].to_i).limit(1).pluck(:last_name).try(:first).to_s
      receive_profile_official_id = Profile.where(:profile_id => params[:profile_id].to_i).limit(1).pluck(:official_id).try(:first).to_s

      @offcial_receiver_identifier = receive_profile_first_name + " " + receive_profile_last_name +"(" +receive_profile_official_id+")"


      

      @get_inbox_contains = Inbox.where(:sent_profile_id => profile_id , :receive_profile_id => params[:profile_id]).or(Inbox.where(:receive_profile_id => profile_id , :sent_profile_id => params[:profile_id]))

      resMain = @get_inbox_contains[0].id.nil? rescue true
      unless resMain
        i = 0
        loop do
          resM = @get_inbox_contains[i].id.nil? rescue true
          unless resM

            @message_list << {receive_message_seen:@get_inbox_contains[i].receive_message_seen,message:@get_inbox_contains[i].message,receive_profile_id:@get_inbox_contains[i].receive_profile_id,sent_profile_id:@get_inbox_contains[i].sent_profile_id}
          end
          i += 1
          if i >= @get_inbox_contains.length.to_i
        
            break       # this will cause execution to exit the loop
          end
        end
      end 


      puts "MESSAGE LIST"
      puts @message_list.inspect


      if  params.include?(:submit_check)

        receive_profile_id =  params["receive_profile_id"]
        unless receive_profile_id.blank?

          first_instance = Inbox.new(:receive_message_seen => 0 ,:message =>inbox_params[:message],:sent_profile_id => profile_id , :receive_profile_id => receive_profile_id)
          first_save = first_instance.save
          #redirect_to inbox_path, notice: 'Thank you!!! Hope you find your match'

        else
          
        end

        


      end
    else
      redirect_to inbox_inboxes_path, notice: 'You are not authorized to sent message to this user'

    end


    








    
  end

  def inbox
    cipher = Gibberish::AES.new('p4ssw0rd')
    client_id = id = cipher.decrypt(session[:client_id])
    profile_id = Profile.where(:client_id => id.to_i).limit(1).pluck(:profile_id).try(:first).to_i
    @message_list =[]

    @get_inbox_contains = Inbox.where(:receive_profile_id => profile_id)

    resMain = @get_inbox_contains[0].id.nil? rescue true
    unless resMain
      i = 0
      loop do
        resM = @get_inbox_contains[i].id.nil? rescue true
        unless resM
          receive_profile_first_name = Profile.where(:profile_id => @get_inbox_contains[i].sent_profile_id.to_i).limit(1).pluck(:first_name).try(:first).to_s
          receive_profile_last_name = Profile.where(:profile_id => @get_inbox_contains[i].sent_profile_id.to_i).limit(1).pluck(:last_name).try(:first).to_s
          receive_profile_official_id = Profile.where(:profile_id => @get_inbox_contains[i].sent_profile_id.to_i).limit(1).pluck(:official_id).try(:first).to_s

          offcial_receiver_identifier = receive_profile_first_name + " " + receive_profile_last_name +"(" +receive_profile_official_id+")"

          @message_list << {offcial_receiver_identifier: offcial_receiver_identifier, receive_message_seen:@get_inbox_contains[i].receive_message_seen,message:@get_inbox_contains[i].message,receive_profile_id:@get_inbox_contains[i].receive_profile_id,sent_profile_id:@get_inbox_contains[i].sent_profile_id}
        end
        i += 1
        if i >= @get_inbox_contains.length.to_i
      
          break       # this will cause execution to exit the loop
        end
      end
    end 
    puts "MESSAGE LIST"
    puts @message_list.inspect
    
  end
 

  

  private


    # Never trust parameters from the scary internet, only allow the white list through.
    def inbox_params

      params.require(:inboxes).permit!
      
    end
end

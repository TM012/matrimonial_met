class ProfileTrustBadgesDocList < ApplicationRecord

	has_attached_file :attach_trust
	validates_attachment :attach_trust, :file_size => {:maximum => 15.5.megabytes.to_i} ,content_type: { content_type: ["application/pdf","application/vnd.ms-excel",     
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/msword", 
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
            "text/plain"] }
end

class Profile < ApplicationRecord


	
	has_attached_file :profile_photo,
   	:default_url => "noprofile_big.png"



   #, :styles => { :small => "160x160>" }
  	validates_attachment :profile_photo, 
    :file_size => {:maximum => 6.5.megabytes.to_i } ,content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
end

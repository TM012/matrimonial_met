Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
 
  resources :profiles do
   collection do
     get :view_profile
     get :search
     get :search_id  
     get :profile
     get :shortlisted
     get :viewed_profile
     get :register
     get :matches
     get :viewed_not_contacted
     get :create_profile
     get :add_profile_picture
     post :add_profile_picture
     get :send_accept_invite
     get :approve_invite
     get :deny_invite
     get :sent_invite
     get :set_preferences
     get :recommended_profiles
     get :prefered_profiles
     get :recent_profiles
     get :premimum_profiles
     get :get_area
     get :profile_search
     get :general_search
     get :shortlisted_profile
     get :upload_verfication_files
     post :upload_verfication_files
     get :set_privacy_option
     get :set_hobby_music
     get :set_package
     get :client_feedback
     get :additional_profile_photos
     post :additional_profile_photos
     get :basic_search



     
     end   
   end
   resources :homes do
      collection do
        get :index


        end
      end





  resources :home do
   collection do
    
     get :contact
     get :members
     get :privacy
     get :terms
     get :upgrade
     get :services
     get :about
     get :faq
     get :shortcodes
     get :index

     end   
 end
 resources :messages do
   collection do
     get :inbox
     get :sent
    
     end   
 end
  resources :authentications do
   collection do
     get :login
     get :register
     
     end   
 end
 resources :inboxes do
      collection do
          get :inbox
          get :sent_message


        end
    end

 
end

   

                 